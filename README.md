# stabFS: A rank-based stable feature selection using Random Forest

The package performs a feature importance based on the ranking of the variables. The function rankBasedFS() calculates the variable importance for all the variables
using conventional random forest importance metrics MDA and MDG in an iterative way. stabFS() function takes the results of rankBasedFS() and calculates different stability 
indexes using Bioconductor package OmicsRankeR: Spearman correlation, Jaccard index, and Kuncheva index

The main function stabFS() needs .gct format for the input predictor matrix, and .cls file indicating the classes of the samples (outcome variable)

Example: To run the example, please use the .gct file "test_data.gct", and the corresponding .cls file "classes.cls". The dataset contains 200 variables and 40 samples (two balanced groups "Normal" and "Tumor")

## Prerequisites
- The following should be installed before launching the tool: randomForest, logging, assertions, tidyassert, caret, ranger, 
  randomForestSRC, RRF, wsrf, ccf, partykit, obliqueRF, PPforest, Rborist, Rerf, RandomUniformForest, wesanderson


## Install 

To use this tool on local machine, simply download the package from gitlab: https://gitlab.com/a.debit/stableFS as a tarball:

And then install the tool

```
install.packages("stabFS.tar.gz", type"source", repos=NULL)
```
Or by using install_gitlab

```
install.packages("remotes")
remotes::install_gitlab("a.debit/stabFS")
```

## Example
We use the dataset provided with the package. The dataset contains the qPCR expression values of 200 miRNAs of 40 samples. The dataset is balanced containing the same amount of tumor and normal classes.
First, we start by performing a first stable FS by running the stabFS() function with and increased values ntree parameter. This function generates a set of .csv files inside the tmp folder. We vizually inspect the results by launching the 
plotStabIdxNtrees() function, which plots stability scores as function of ntree and variable cardinality. From this plot we can select a combination cardinality:ntree = (Nv:Nt) values which maximizes the stability scores.
We use this combination in downstream analysis to optimize the parameters.  

```
library(stabFS)
```
perform a stable feature selection by calling the function stabFS() 
```
dataGCTFile=system.file("extdata", "test_data.gct", package = "stabFS") 
classCLSFile=system.file("extdata", "classes.cls", package = "stabFS")
# read gct file (data)
dataImport=read.gct(dataGCTFile)$data
#read the .cls file (class information)
classInf=read.cls(classCLSFile)
#run stable feature selection using different number of trees
l_ntree=seq(50,500,by=50)
lapply(l_ntree, function(nt){ 
	stabFS(dataGCTFile, classCLSFile, nTrees = nt, nPartitions = 10, bstRate = .9, Ratio = 1.1)
})	
```
After completion with success, the results will be stored inside the tmp folder
```
#plotting stablity indexes as function of ntree and sequence cardinality, for multiple runs of stabFS with different values of ntree
Nv = c(10,20,50,100,150,200)

plotStabIdxNtrees(l_ntree=l_ntree, Nv)

```
From the plots extract the optimal values of variable cardinality and the parameter ntree
```
#from the generated plot, extract number of trees Nt and sequence cardinality Nv maximizing the stability indexes
Nt=150
Nv=200

#plotting stability as function of the first Nv optimal variables and using an optimal number of trees Nt 

plotStabIdxnVar(nbreVarOpt=Nv, ntreesOpt=Nt)
```
plot the corresponding boxplot rank of top Nv variables
``` 
#plot the corresponding boxplots ranks of the first Nv variables

plotRanking(nbreVarOpt=Nv, ntreesOpt=Nt)
```
Calculates the out-of-bag error and plot to optimize the parameter ntree
```
# plot OOB Error vs ntree
dataGCTFile=system.file("extdata", "test_data.gct", package = "stabFS") 
classCLSFile=system.file("extdata", "classes.cls", package = "stabFS")
ntrees=seq(20, 100, 10)
#generate a data.frame containing the ooberrors of the methods (rows) as function of the variation of ntree parameter (columns)
rf_methods=c("cforest", "obliquerf", "ppforest", "ruf", "randomforest","ranger", "rrf", "rborist", "rfsrc", "rerf", "wsrf") # Rf methods included are: randomforest, ranger, rrf, wsrf, rfsrc, cforest, rborist, ppforest, rerf, ruf, obliquerf
df_ooberros=do.call(cbind, pbapply::pblapply(ntrees, function(ntree){
	getOOBError(dataGCTFile, classCLSFile, opt=10, ntree=ntree, rf_methods=rf_methods)
}))
plotOOBError(df_ooberros, yl=0.5, legendPosition="topright", l_ntree=ntrees)
	
```

# How to cite this material

Debit, Ahmed (2023). A stable feature selection based on random forest and stability measures.. figshare. Software. https://doi.org/10.6084/m9.figshare.24878646.v1

# Publication

Poulet, C., Debit, A., Josse, C., Jerusalem, G., Azencott, C.A., Bours, V. and Van Steen, K., 2023. Assessing Random Forest self-reproducibility for optimal short biomarker signature discovery. biorxiv, pp.2023-03.

# Authors

* **Ahmed Debit** - Bioinformatician and Computational Biologist IBENS Paris, GIGA-Uliege Liege